/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./assets/**/*.js",
    "./templates/**/*.html.twig",
    "./node_modules/flowbite/**/*.js"
  ],
  theme: {
    screens: {

      'screen': {'min': '1401px'},
      // => @media (max-width: 1920px) { ... }

      'other': {'max': '1500px'},

      'laptop': {'max': '1440px'},
      // => @media (max-width: 1400px) { ... }

      'smLaptop': {'max': '1024px'},
      // => @media (max-width: 1024px) { ... }

      'smScreen': {'max': '940px'},

      'tablet': {'max': '768px'},
      // => @media (max-width: 768px) { ... }

      'phone': {'max': '475px'},
      // => @media (max-width: 475px) { ... }

      'smPhone': {'max': '450px'}
    },
    extend: {},
  },
  plugins: [
    require('tailwind-scrollbar'),
    require('flowbite/plugin'),
    require("daisyui"),
  ],
}
