import {Controller} from "@hotwired/stimulus";

export default class extends Controller {

    async connect() {
        this.url = this.element.dataset.src;

        console.log(this.url);

        const response = await fetch(this.url)
        const result = await response.text();
        const dest = this.element
        dest.innerHTML = result;
    }

    disconnect() {
    }
}