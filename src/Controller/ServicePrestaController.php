<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Security\MainAuthenticator;
use App\Service\RemoteDataService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class ServicePrestaController extends AbstractController
{
    #[Route('/services', name: 'app_servicepresta')]
    public function index(RemoteDataService $remoteDataService): Response
    {
        return $this->render('front/servicesPresta.html.twig', ["Prestations" => $remoteDataService->getData()]);
    }
}