<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReservationController extends AbstractController
{
    #[Route('/reservation', name: 'app_reservationpresta')]
    public function selectionDePrestation(Request $request): Response {

        $datePresta = [];
        $dateObject = [];
        $days = [];
        $currentDate = new \DateTimeImmutable();

        $one_day_after = new \DateInterval('P1D');

        for($i = 1; $i < 31; $i++)
        {
            $dateObject[] = $currentDate = $currentDate->add($one_day_after);
        }

        foreach ($dateObject as $date)
        {
            $datePresta[] = $date;
        }

        $prestationSelect = [
            'id' => $request->get('id'),
            'label' => $request->get('label'),
            'duree' => $request->get('duree'),
            'prix' => $request->get('prix')
        ];

        return $this->render('front/reservatonPresta.html.twig', ['prestation' => $prestationSelect,
                                                                        'allDate'=>$datePresta]);
    }
}