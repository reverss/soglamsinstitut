import { Controller}  from "@hotwired/stimulus";
import * as $ from 'jquery'

export default class extends Controller
{

    connect() {
    }

    async onSubmit(event) {
        event.preventDefault()

        const $form = $('#' + this.element.target).find('form')

        try {
            await $.ajax({
                    url: $form.prop('action'),
                    method: $form.prop('method'),
                    type: $form.prop('method'),
                    data: $form.serialize()
                }
            ).then((response) => {
                if (response.url) {
                    window.location.href = response.url
                } else {
                    const target = document.querySelector('#' + this.element.target);
                    target.innerHTML = response;
                }

            })

        } catch(e) {
            const target = document.querySelector('#' + this.element.target);
            target.innerHTML =  e.responseText
        }

    }

}