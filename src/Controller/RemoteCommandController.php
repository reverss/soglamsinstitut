<?php

namespace App\Controller;

use App\Security\MainAuthenticator;
use App\Service\RemoteDataService;
use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class RemoteCommandController extends AbstractController
{
    #[Route('/internal/remote', name: 'internal')]
    public function index(RemoteDataService $dataService, Request $request): Response
    {
        if($request->getMethod() == "POST")
        {
            $json = $request->get('json');

            $dataService->setDataRemote($json);

            return new RedirectResponse('/');
        }

        $response = new Response();
        $response->setStatusCode(500);

        return $response;

    }
}