<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\MainAuthenticator;
use App\Service\CryptoService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class RegisterController extends AbstractController
{
    public function __construct(private EntityManagerInterface $entityManager,
                                private UserPasswordHasherInterface $passwordHasher,
                                private MailerInterface $mailer,
                                private CryptoService $cryptoService)
    {
    }


    #[Route('/register', name: 'app_register')]
    public function index(Request $request, UserAuthenticatorInterface $userAuthenticator, MainAuthenticator $mainAuthenticator): Response
    {
        $errorsMail = [];
        $errorsPwd = [];

        if ($request->isMethod('POST'))
        {
            $email = $request->get('email');
            $pass = $request->get('password');
            $passC = $request->get('passwordC');

            if($email == "")
            {
                $errorsMail[] = "L'email est obligatoire !";
            }
            if(null !== $this->entityManager->getRepository(User::class)->findOneByEmail($email))
            {
                $errorsMail[] = "L'email est déjà utilisé !";
            }
            if($pass != $passC || $passC == "")
            {
                $errorsPwd[] = "Les deux mot de passe ne correspondent pas !";
            }
            if (strlen($pass) < 6) {
                $errorsPwd[] = "6 caractères minium requis";
            }
            if (count($errorsPwd) == 0 && count($errorsMail) == 0)
            {
                $user = new User();

                $user->setEmail($email);
                $user->setRoles(array('ROLE_NOTVERIF'));

                $passHashed = $this->passwordHasher->hashPassword($user, $pass);
                $user->setPassword($passHashed);

                $this->entityManager->persist($user);
                $this->entityManager->flush($user);

                $token = [
                    'id' => $user->getId(),
                    'ml' => $user->getEmail(),
                    'st' => 'dazdazdd',
                    'date' => (new \DateTimeImmutable('+1 week'))->format('U')
                ];

                $encToken = $this->cryptoService->crypt64(json_encode($token));

                $url = $this->generateUrl('app_checkmail', ['token' => $encToken], UrlGeneratorInterface::ABSOLUTE_URL);

                $mail = (new TemplatedEmail())
                    ->from(new Address("noreply@soglamsinstitut.fr", "So Glam's Institut"))
                    ->to($email)
                    ->subject('Mail de confirmation de votre compte !')

                    ->htmlTemplate('mail/verificationMail.html.twig')
                    ->context(['url' => $url]);

                $this->mailer->send($mail);

                $userAuthenticator->authenticateUser($user, $mainAuthenticator, $request);
                return $this->redirectToRoute('app_home');
            }
        }
        return $this->render('front/register.html.twig', [
            'errorsMail' => $errorsMail,
            'errorsPwd' => $errorsPwd
        ]);
    }

    #[Route('/checkMail', name:'app_checkmail')]
    public function checkEmail(Request $request, UserAuthenticatorInterface $userAuthenticator, MainAuthenticator $mainAuthenticator) {

        $errorsLink = [];

        $token = $request->get('token');
        $ct = $this->cryptoService->decrypt64($token);
        $dct = (array) json_decode($ct);

        $dateExp = (new \DateTime())->setTimestamp($dct['date']);


        if(count($dct) == 0)
        {
            $errorsLink[] = "Lien introuvable";
        }

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)->findOneByEmail($dct['ml']);

        if($user !== null)
        {
            if($user->getDateValidationMail() !== null)
            {
                $errorsLink[] = 'Compte déjà validé';
            }

            if($dateExp < new \DateTime())
            {
                $errorsLink[] = 'Lien expiré';
             }
        }

        if(count($errorsLink) == 0)
        {
            $user->setDateValidationMail(new \DateTimeImmutable());

            $roles = $user->getRoles();

            for($i = 0; $i < count($roles); $i++ )
            {
                if($roles[$i] == "ROLE_NOTVERIF")
                {
                    unset($roles[$i]);
                }
            }

            $user->setRoles($roles);
            $this->entityManager->persist($user);
            $this->entityManager->flush($user);

            $userAuthenticator->authenticateUser($user, $mainAuthenticator, $request);
            return $this->redirectToRoute('app_home');
        }

        return $this->render('mail/confMail.html.twig', [
            'errors' => $errorsLink
        ]);
    }
}
