<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use function Symfony\Component\String\u;

class ProfilController extends AbstractController
{
    #[Route('/profil', name: 'app_profil')]
    public function index() {
        return $this->render('front/profil.html.twig');
    }
}