<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\CartItem;
use App\Entity\Prestation;
use App\Entity\TypePrestation;
use App\Service\RemoteDataService;
use Doctrine\ORM\EntityManagerInterface;
use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class CartController extends AbstractController
{
    public function __construct(private EntityManagerInterface $entityManager,
                                private SessionInterface $session,
                                private Security $security)
    {
    }

    #[Route('/cart/init', name: 'intern_cart_init')]

    private function CartInit($creation = false)
    {
        $currentSession = $this->session->get("app_cart");

        if($currentSession == null)
        {
            $cart = $this->CreateCart();

        }else {

            $cart = $this->entityManager->getRepository(Cart::class)
                ->findOneByUuidStatut($currentSession, "NEW");
            if($cart == null)
            {
                $cart = $this->CreateCart();
            }
        }

        if ($this->security->getToken() !== null &&  $this->security->getToken()->getUser() !== null
                                                  && $cart->getUser() == null)
        {
            $cart->setUser($this->security->getToken()->getUser());
            $this->entityManager->persist($cart);
            $this->entityManager->flush($cart);
        }

        return $this->json($cart);
    }

    public function CreateCart() {

        $cart = new Cart();
        $this->entityManager->persist($cart);
        $this->entityManager->flush($cart);

        $this->session->set("app_cart", $cart->getUuid());

        return $cart;
    }

    #[Route('/cart/add', name: 'intern_add_item_cart')]
    public function addItemCart(RemoteDataService $dataService) {

        $prestationTab = [];


        $prestationTab = [  ["id"=>1,
                            "Reference"=>"GelBase",
                            "Label"=>"Gel Base Renforce Naturel + Couleur",
                            "Description"=>"mdmdmdmddmdm",
                            "Prix"=>40,
                            "Duree"=>"1h30",
                            "TypePrestation"=>"Onglerie"],

                            ["id"=>2,
                            "Reference"=>"GelBase",
                            "Label"=>"Gel Base Renforce Naturel + Couleur",
                            "Description"=>"adadsdsdasdazd",
                            "Prix"=>40,
                            "Duree"=>"1h25",
                            "TypePrestation"=>"Onglerie"]];

        $json = json_encode($prestationTab);

        $dataService->setDataRemote($json);

        return new RedirectResponse("/");
    }
}