<?php

namespace App\Service;

class CryptoService
{
    private $key;

    public function __construct()
    {
        $this->key = 'soglam';
    }


    public function crypt64($str, $salt=null)
    {
        return base64_encode($this->crypt($str, $salt));
    }

    public function decrypt64($str, $salt=null)
    {
        return $this->decrypt(base64_decode($str),$salt);
    }

    private function crypt($str, $salt=null)
    {
        return openssl_encrypt($str, "AES-256-ECB" ,($salt?:'').$this->key);
    }

    private function decrypt($str, $salt=null)
    {
        return openssl_decrypt($str, "AES-256-ECB" ,($salt?:'').$this->key);
    }
}