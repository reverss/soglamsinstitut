<?php

namespace App\Service;

use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;

class RemoteDataService
{
    public function __construct(private KernelInterface $kernel)
    {
    }
    public function setDataRemote($json)
    {
        $racine = $this->kernel->getProjectDir();
        $dir = $racine . "/public/assets/json/";

        $filesystem = new Filesystem();

        if(!$filesystem->exists($dir))
        {
            try{
                $filesystem->mkdir($dir, 0777);
                $filesystem->touch($dir . 'prestations.json');
            }catch (IOExceptionInterface $exception)
            {
                dd($exception->getPath());
            }
        }else if($filesystem->exists($dir . 'prestations.json'))
        {
            try {
                file_put_contents($dir . 'prestations.json', $json);
            }catch (\Exception $e)
            {
                echo($e->getMessage());
            }
        }

    }

    public function getData(){

        $racine = $this->kernel->getProjectDir();
        $dir = $racine . "/public/assets/json/";

        $json = file_get_contents($dir . 'prestations.json');

        return json_decode($json);

    }
}