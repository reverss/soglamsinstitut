<?php

namespace App\Service;

use App\Entity\CheckLogin;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class VerifLoginService
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
        $this->ip = $_SERVER["REMOTE_ADDR"];

    }

    public function isLocked(User $user): bool
    {
        // Clear expired
        $conn = $this->entityManager->getConnection();

        $conn->executeQuery("DELETE FROM check_login
                WHERE date_exp <= CURRENT_TIME()");

        // Verification si il doit être lock
        $repo = $this->entityManager->getRepository(CheckLogin::class);
        /** @var CheckLogin $lock */

        $lock = $repo->findOneBy([
            'identifiant' => $user->getEmail(),
            'addr' => $this->ip,
            'tentativeRest' => 0
        ]);

        if ($lock)
        {
            return true;
        }

        return false;
    }

    public function addFail(User $user): ?bool
    {
        $repo = $this->entityManager->getRepository(CheckLogin::class);

        $userLogging= $repo->findOneBy([
            'identifiant' => $user->getEmail(),
            'addr' => $this->ip,
        ]);

        if($userLogging != null && $userLogging->getTentativeRest() > 0)
        {
            $tentative = $userLogging->getTentativeRest();
            $userLogging->setTentativeRest($tentative - 1);
            if ($userLogging->getTentativeRest()==0) {
                $userLogging->setDateExp(new \DateTimeImmutable("+30min"));

            } else {
                $userLogging->setDateExp(new \DateTimeImmutable("+10min"));

            }

            $this->entityManager->persist($userLogging);
            $this->entityManager->flush($userLogging);
        }
        elseif ($userLogging != null && $userLogging->getTentativeRest() == 0)
        {
            return $this->isLocked($user);
        }
        else
        {
            $checkLogin = new CheckLogin();

            $checkLogin->setAddr($this->ip);
            $checkLogin->setIdentifiant($user->getEmail());
            $checkLogin->setTentativeRest(5);
            $checkLogin->setDateExp(new \DateTimeImmutable());

            $this->entityManager->persist($checkLogin);
            $this->entityManager->flush();
        }
        return false;
    }

    public function getTentativeLeft(User $user): ?int
    {
        /** @var CheckLogin $checkLogin */

        $checkLogin = $this->entityManager->getRepository(CheckLogin::class)->findOneBy([
            'identifiant' => $user->getEmail(),
            'addr' => $this->ip
        ]);

        if ($checkLogin) {
            return $checkLogin->getTentativeRest();
        }
        return 5;
    }
}