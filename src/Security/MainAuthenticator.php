<?php

namespace App\Security;

use App\Entity\CheckLogin;
use App\Entity\User;
use App\Service\VerifLoginService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;

class MainAuthenticator extends AbstractAuthenticator
{
    public function __construct(private RouterInterface $router, private EntityManagerInterface $entityManager, private VerifLoginService $verifLogin, private MailerInterface $mailer)
    {
    }

    public function supports(Request $request): ?bool
    {
        // TODO: Implement supports() method.
        if ($request->getMethod()=='POST'
            && $request->attributes->get('_route') == 'app_connexion'
            && ""!==$request->get('email',"")
            && ""!==$request->get('password',"")
        ) {
            return true;
        }
        return false;
    }

    public function authenticate(Request $request): Passport
    {
        $email = $request->get('email');
        $pass = $request->get('password');
        return new Passport(new UserBadge($email), new PasswordCredentials($pass));

    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        /** @var User $user */
        $user = $token->getUser();
        $email = $request->get('email');

        $conn = $this->entityManager->getConnection();

        $conn->executeQuery('DELETE FROM check_login WHERE addr = :addr AND identifiant = :identifiant', [
            'addr' => $this->ip = $_SERVER["REMOTE_ADDR"],
            'identifiant' => $email
        ]);

        $user->setDateLastLogin();

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return new RedirectResponse('/');
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        //Test if user exists
        $identifiant = $request->get('email');
        $userRepository = $this->entityManager->getRepository(User::class);
        /** @var User $user */
        $user = $userRepository->findOneByEmail($identifiant);

        if($user)
        {
            $this->verifLogin->addFail($user);
        }

        if ($request->hasSession()) {
            $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
            $request->getSession()->set(Security::LAST_USERNAME, $identifiant);
            if ($user) {
                $request->getSession()->set("loginFailedCompteur", $this->verifLogin->getTentativeLeft($user));
            }
            else
            {
                $request->getSession()->set("loginFailedCompteur", 100);
            }
        }
        return null;
    }

//    public function start(Request $request, AuthenticationException $authException = null): Response
//    {
//        /*
//         * If you would like this class to control what happens when an anonymous user accesses a
//         * protected page (e.g. redirect to /login), uncomment this method and make this class
//         * implement Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface.
//         *
//         * For more details, see https://symfony.com/doc/current/security/experimental_authenticators.html#configuring-the-authentication-entry-point
//         */
//    }
}
